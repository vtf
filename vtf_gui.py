# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'vtf.ui'
#
# Created: Tue Jan 29 17:29:51 2008
#      by: PyQt4 UI code generator 4.3.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

class Ui_Vtf(object):
    def setupUi(self, Vtf):
        Vtf.setObjectName("Vtf")
        Vtf.resize(QtCore.QSize(QtCore.QRect(0,0,276,308).size()).expandedTo(Vtf.minimumSizeHint()))

        self.vboxlayout = QtGui.QVBoxLayout(Vtf)
        self.vboxlayout.setObjectName("vboxlayout")

        self.groupBox = QtGui.QGroupBox(Vtf)
        self.groupBox.setObjectName("groupBox")

        self.gridlayout = QtGui.QGridLayout(self.groupBox)
        self.gridlayout.setObjectName("gridlayout")

        self.label = QtGui.QLabel(self.groupBox)
        self.label.setObjectName("label")
        self.gridlayout.addWidget(self.label,0,0,1,1)

        self.SP_Denti = QtGui.QSpinBox(self.groupBox)
        self.SP_Denti.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.SP_Denti.setMinimum(1)
        self.SP_Denti.setObjectName("SP_Denti")
        self.gridlayout.addWidget(self.SP_Denti,0,1,1,1)

        self.label_2 = QtGui.QLabel(self.groupBox)
        self.label_2.setObjectName("label_2")
        self.gridlayout.addWidget(self.label_2,2,0,1,1)

        self.LE_VelTaglio = QtGui.QLineEdit(self.groupBox)
        self.LE_VelTaglio.setMaxLength(8)
        self.LE_VelTaglio.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.LE_VelTaglio.setObjectName("LE_VelTaglio")
        self.gridlayout.addWidget(self.LE_VelTaglio,2,1,1,1)

        self.label_3 = QtGui.QLabel(self.groupBox)
        self.label_3.setObjectName("label_3")
        self.gridlayout.addWidget(self.label_3,2,2,1,1)

        self.label_4 = QtGui.QLabel(self.groupBox)
        self.label_4.setObjectName("label_4")
        self.gridlayout.addWidget(self.label_4,3,0,1,1)

        self.LE_AvaDente = QtGui.QLineEdit(self.groupBox)
        self.LE_AvaDente.setMaxLength(7)
        self.LE_AvaDente.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.LE_AvaDente.setObjectName("LE_AvaDente")
        self.gridlayout.addWidget(self.LE_AvaDente,3,1,1,1)

        self.label_5 = QtGui.QLabel(self.groupBox)
        self.label_5.setObjectName("label_5")
        self.gridlayout.addWidget(self.label_5,3,2,1,1)

        self.label_6 = QtGui.QLabel(self.groupBox)
        self.label_6.setObjectName("label_6")
        self.gridlayout.addWidget(self.label_6,1,0,1,1)

        self.label_7 = QtGui.QLabel(self.groupBox)
        self.label_7.setObjectName("label_7")
        self.gridlayout.addWidget(self.label_7,1,2,1,1)

        spacerItem = QtGui.QSpacerItem(40,20,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum)
        self.gridlayout.addItem(spacerItem,0,2,1,1)

        self.LE_Diametro = QtGui.QLineEdit(self.groupBox)
        self.LE_Diametro.setMaxLength(7)
        self.LE_Diametro.setCursorPosition(0)
        self.LE_Diametro.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.LE_Diametro.setObjectName("LE_Diametro")
        self.gridlayout.addWidget(self.LE_Diametro,1,1,1,1)
        self.vboxlayout.addWidget(self.groupBox)

        self.groupBox_2 = QtGui.QGroupBox(Vtf)
        self.groupBox_2.setObjectName("groupBox_2")

        self.gridlayout1 = QtGui.QGridLayout(self.groupBox_2)
        self.gridlayout1.setObjectName("gridlayout1")

        self.label_8 = QtGui.QLabel(self.groupBox_2)
        self.label_8.setObjectName("label_8")
        self.gridlayout1.addWidget(self.label_8,0,0,1,1)

        self.label_12 = QtGui.QLabel(self.groupBox_2)
        self.label_12.setObjectName("label_12")
        self.gridlayout1.addWidget(self.label_12,0,2,1,1)

        self.label_9 = QtGui.QLabel(self.groupBox_2)
        self.label_9.setObjectName("label_9")
        self.gridlayout1.addWidget(self.label_9,1,0,1,1)

        self.label_13 = QtGui.QLabel(self.groupBox_2)
        self.label_13.setObjectName("label_13")
        self.gridlayout1.addWidget(self.label_13,1,2,1,1)

        self.LE_Giri = QtGui.QLineEdit(self.groupBox_2)
        self.LE_Giri.setAutoFillBackground(False)
        self.LE_Giri.setMaxLength(32767)
        self.LE_Giri.setEchoMode(QtGui.QLineEdit.Normal)
        self.LE_Giri.setCursorPosition(1)
        self.LE_Giri.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.LE_Giri.setReadOnly(False)
        self.LE_Giri.setObjectName("LE_Giri")
        self.gridlayout1.addWidget(self.LE_Giri,0,1,1,1)

        self.LE_Avanzamento = QtGui.QLineEdit(self.groupBox_2)
        self.LE_Avanzamento.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.LE_Avanzamento.setReadOnly(False)
        self.LE_Avanzamento.setObjectName("LE_Avanzamento")
        self.gridlayout1.addWidget(self.LE_Avanzamento,1,1,1,1)
        self.vboxlayout.addWidget(self.groupBox_2)

        spacerItem1 = QtGui.QSpacerItem(20,40,QtGui.QSizePolicy.Minimum,QtGui.QSizePolicy.Expanding)
        self.vboxlayout.addItem(spacerItem1)

        self.hboxlayout = QtGui.QHBoxLayout()
        self.hboxlayout.setObjectName("hboxlayout")

        spacerItem2 = QtGui.QSpacerItem(40,20,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Minimum)
        self.hboxlayout.addItem(spacerItem2)

        self.B_Quit = QtGui.QPushButton(Vtf)
        self.B_Quit.setAutoDefault(False)
        self.B_Quit.setFlat(False)
        self.B_Quit.setObjectName("B_Quit")
        self.hboxlayout.addWidget(self.B_Quit)
        self.vboxlayout.addLayout(self.hboxlayout)

        self.retranslateUi(Vtf)
        QtCore.QObject.connect(self.B_Quit,QtCore.SIGNAL("clicked()"),Vtf.accept)
        QtCore.QMetaObject.connectSlotsByName(Vtf)
        Vtf.setTabOrder(self.SP_Denti,self.LE_Diametro)
        Vtf.setTabOrder(self.LE_Diametro,self.LE_VelTaglio)
        Vtf.setTabOrder(self.LE_VelTaglio,self.LE_AvaDente)
        Vtf.setTabOrder(self.LE_AvaDente,self.LE_Giri)
        Vtf.setTabOrder(self.LE_Giri,self.LE_Avanzamento)
        Vtf.setTabOrder(self.LE_Avanzamento,self.B_Quit)

    def retranslateUi(self, Vtf):
        Vtf.setWindowTitle(QtGui.QApplication.translate("Vtf", "Velocità Taglio Frese Ver. 0.1", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox.setTitle(QtGui.QApplication.translate("Vtf", "Parametri utensile", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Vtf", "Numero Denti", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("Vtf", "Velocità taglio", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("Vtf", "m/min", None, QtGui.QApplication.UnicodeUTF8))
        self.label_4.setText(QtGui.QApplication.translate("Vtf", "Avanzamento", None, QtGui.QApplication.UnicodeUTF8))
        self.label_5.setText(QtGui.QApplication.translate("Vtf", "mm/dente", None, QtGui.QApplication.UnicodeUTF8))
        self.label_6.setText(QtGui.QApplication.translate("Vtf", "Diametro", None, QtGui.QApplication.UnicodeUTF8))
        self.label_7.setText(QtGui.QApplication.translate("Vtf", "mm", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_2.setTitle(QtGui.QApplication.translate("Vtf", "Parametri macchina", None, QtGui.QApplication.UnicodeUTF8))
        self.label_8.setText(QtGui.QApplication.translate("Vtf", "Numero di giri", None, QtGui.QApplication.UnicodeUTF8))
        self.label_12.setText(QtGui.QApplication.translate("Vtf", "giri/min", None, QtGui.QApplication.UnicodeUTF8))
        self.label_9.setText(QtGui.QApplication.translate("Vtf", "Avanzamento", None, QtGui.QApplication.UnicodeUTF8))
        self.label_13.setText(QtGui.QApplication.translate("Vtf", "mm/min", None, QtGui.QApplication.UnicodeUTF8))
        self.LE_Giri.setText(QtGui.QApplication.translate("Vtf", "0", None, QtGui.QApplication.UnicodeUTF8))
        self.LE_Avanzamento.setText(QtGui.QApplication.translate("Vtf", "0", None, QtGui.QApplication.UnicodeUTF8))
        self.B_Quit.setText(QtGui.QApplication.translate("Vtf", "&Quit", None, QtGui.QApplication.UnicodeUTF8))
        self.B_Quit.setShortcut(QtGui.QApplication.translate("Vtf", "Ctrl+Q", None, QtGui.QApplication.UnicodeUTF8))

