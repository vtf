#!/usr/bin/env python

# system include
import sys, os, re

# PyQt4 include
from PyQt4 import QtGui, QtCore

# Specific include
from vtf_gui import Ui_Vtf

class VtfWindow(Ui_Vtf):
    def __init__(self):
        self.giri = 0
        self.denti = 1
        self.ava_dente = 0.0
        self.vel_ava = 0.0
        self.diametro = 0.0
        self.vel_taglio = 0.0
        
    def FinalizeGui(self):
        QtCore.QObject.connect(ui.SP_Denti, QtCore.SIGNAL("valueChanged (int)"), self.DentiMod)
        QtCore.QObject.connect(ui.LE_Diametro, QtCore.SIGNAL("textChanged (QString)"), self.DiamMod)
        QtCore.QObject.connect(ui.LE_VelTaglio, QtCore.SIGNAL("textChanged (QString)"), self.VelTaglioMod)
        QtCore.QObject.connect(ui.LE_AvaDente, QtCore.SIGNAL("textChanged (QString)"), self.AvaDentMod)
        
    def UpdateVel(self):
        # Calcolo il numero di giri
        # Controllo che tutti i valori necessari siano diversi da zero
        if self.vel_taglio == 0 or self.diametro == 0:
            giri_min = 0
        else:
            giri_min = int((self.vel_taglio * 1000) / self.diametro / 3.14)
        ui.LE_Giri.setText(str(giri_min))
        self.giri = giri_min
        self.UpdateAv()
    
    def UpdateAv(self):
        if self.giri == 0 or self.ava_dente == 0:
            ava_min = 0
        else:
            ava_min = self.giri * self.denti * self.ava_dente
        ui.LE_Avanzamento.setText(str(ava_min))
    
    def DentiMod(self, i):
        if i == 0:
            return
        self.denti = i
        self.UpdateVel()
    
    def DiamMod(self, text):
        try:
            self.diametro = float(text)
            self.UpdateVel()
        except:
            ui.LE_Diametro.setText(text[:-1])
            
    def VelTaglioMod(self, text):
        try:
            self.vel_taglio = float(text)
            self.UpdateVel()
        except:
            ui.LE_VelTaglio.setText(text[:-1])
                    
    def AvaDentMod(self, text):
        try:
            self.ava_dente = float(text)
            self.UpdateVel()
        except:
            ui.LE_AvaDente.setText(text[:-1])



app = QtGui.QApplication(sys.argv)
window = QtGui.QDialog()
ui = VtfWindow()
ui.setupUi(window)
ui.FinalizeGui()
window.show()
sys.exit(app.exec_())



